#include <stdlib.h>
#include <string.h>

#include <scroller.h>

#define BRIGHT_SCALE(component, level) (((level) * (component)) / 256)

struct scr {
	struct scr_cfg cfg;
	uint32_t screen_width_bytes;
	uint32_t map_width_bytes;
	uint32_t brightness;
	uint32_t x; // Scroll coordinate x
	uint32_t y; // Scroll coordinate y
};

void scr_config_set(scr_handle scr, const struct scr_cfg *cfg)
{
	scr->cfg = *cfg;
	if (!cfg->wrap_width) {
		scr->cfg.wrap_width = cfg->map.width;
	}
	scr->screen_width_bytes = 3 * cfg->screen.width;
	scr->map_width_bytes = 3 * cfg->map.width;
}

scr_handle scr_init(const struct scr_cfg *cfg)
{
	scr_handle scr = calloc(1, sizeof(struct scr));
	if (!scr) {
		return NULL;
	}

	scr_config_set(scr, cfg);
	// Set brightness to a low level
	scr->brightness = 16;

	return scr;
}

void scr_deinit(scr_handle scr)
{
	free(scr);
}

static void copy_rect(scr_handle scr, uint32_t off, const uint8_t *org,
		uint32_t scr_width, uint32_t height)
{
	uint8_t *dst = scr->cfg.screen_buf;

	if (!scr->cfg.px_order) {
		dst += off * 3;
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t j = 0; j < scr_width * 3; j++) {
				dst[j] = BRIGHT_SCALE(org[j], scr->brightness);
			}
			dst += scr->screen_width_bytes;
			org += scr->map_width_bytes;
		}
	} else {
		for (uint32_t i = 0; i < height; i++) {
			for (uint32_t j = 0, pos = 0; j < scr_width; j++) {
				// Each inner iteration is a pixel --> 3 bytes
				uint32_t byte = scr->cfg.px_order[j + off];
				dst[byte++] = BRIGHT_SCALE(org[pos++],
						scr->brightness);
				dst[byte++] = BRIGHT_SCALE(org[pos++],
						scr->brightness);
				dst[byte] = BRIGHT_SCALE(org[pos++],
						scr->brightness);
			}
			off += scr->cfg.screen.width;
			org += scr->map_width_bytes;
		}
	};
}

static void no_wrap_copy(scr_handle scr, uint32_t x, uint32_t y)
{
	const uint8_t *org = scr->cfg.map_buf +
		scr->map_width_bytes * y + 3 * x;

	copy_rect(scr, 0, org, scr->cfg.screen.width,
			scr->cfg.screen.height);
}

static void x_wrap_copy(scr_handle scr, uint32_t off, uint32_t x, uint32_t y,
		uint32_t margin, uint32_t scr_height)
{
	const uint8_t *line = scr->cfg.map_buf + scr->map_width_bytes * y;

	copy_rect(scr, off, line + 3 * x, margin, scr_height);
	copy_rect(scr, off + margin, line,
			scr->cfg.screen.width - margin, scr_height);
}

static void y_wrap_copy(scr_handle scr, uint32_t x, uint32_t y, uint32_t margin)
{
	const uint8_t *line = scr->cfg.map_buf + scr->map_width_bytes * y;

	copy_rect(scr, 0, line + 3 * x, scr->cfg.screen.width, margin);
	copy_rect(scr, scr->cfg.screen.width * margin,
			scr->cfg.map_buf + 3 * x,
			scr->cfg.screen.width, scr->cfg.screen.height - margin);
}

static void xy_wrap_copy(scr_handle scr, uint32_t x, uint32_t y,
		uint32_t x_margin, uint32_t y_margin)
{
	x_wrap_copy(scr, 0, x, y, x_margin, y_margin);
	x_wrap_copy(scr, scr->cfg.screen.width * y_margin, x, 0,
			x_margin, scr->cfg.screen.height - y_margin);
}

void scr_draw(scr_handle scr)
{
	uint32_t x = scr->x;
	uint32_t y = scr->y;
	int32_t x_margin = scr->cfg.map.width - x;
	int32_t y_margin = scr->cfg.map.height - y;
	uint_fast8_t flags = 0;

	if (x_margin < scr->cfg.screen.width) {
		flags |= 1;
	}
	if (y_margin < scr->cfg.screen.height) {
		flags |= 2;
	}

	switch (flags) {
		case 0: // No wrap
			no_wrap_copy(scr, x, y);
			break;

		case 1: // Wrap on x axis
			x_wrap_copy(scr, 0, x, y, x_margin,
					scr->cfg.screen.height);
			break;

		case 2: // Wrap on y axis
			y_wrap_copy(scr, x, y, y_margin);
			break;

		case 3:	// Wrap on both x and y axes
			xy_wrap_copy(scr, x, y, x_margin, y_margin);
			break;
	}

}

struct scr_cfg *scr_cfg_get(scr_handle scr)
{
	return &scr->cfg;
}

void scr_px_order_set(scr_handle scr, const uint32_t *px_order)
{
	scr->cfg.px_order = px_order;
}

void scr_scroll_set(scr_handle scr, uint32_t x, uint32_t y)
{
	if (x >= scr->cfg.wrap_width) {
		x %= scr->cfg.wrap_width;
	}
	if (y >= scr->cfg.map.height) {
		y %= scr->cfg.map.height;
	}

	scr->x = x;
	scr->y = y;
}

void scr_scroll_get(scr_handle scr, uint32_t *x, uint32_t *y)
{
	*x = scr->x;
	*y = scr->y;
}

uint_fast8_t scr_brightness_get(scr_handle scr)
{
	return scr->brightness - 1;
}

void scr_brighthess_set(scr_handle scr, uint_fast8_t level)
{
	scr->brightness = level + 1;
}
