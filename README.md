# Introduction

This component implements a framebuffer rendering with scroll and pixel transformation capabilities. It is prepared to be used with Espressif SDKs ([ESP8266\_RTOS\_SDK](https://github.com/espressif/ESP8266_RTOS_SDK) and [esp-idf](https://github.com/espressif/esp-idf)), but its implementation is pretty generic and has no dependencies other than standard library functions, so you should be able to use it with any device having a 24-bit framebuffer (3 bytes per pixel).

# Usage

You can see an example on how to use this module along with the [simple\_txt](https://gitlab.com/doragasu/simple_txt) module in the [m5am\_text\_scroll](https://gitlab.com/doragasu/m5am_text_scroll) project.

## Basic usage

1. Set up the map buffer with whatever you want to display. It has to be 24-bits per pixel (but format does not matter, it can be RGB, GRB, or whatever).
2. Set up the screen buffer. It must have the same formatting than the map buffer (24-bit per pixel and the same byte ordering). The screen buffer is typically smaller than the map buffer.
3. Configure the scroller calling `scr_init()`.
4. Anytime you want the scroller to render to the screen buffer, call `scr_draw()` with the appropriate `x` and `y` scroll values.

## Advanced usage

There is an advanced feature allowing to change the ordering used to draw pixels on the screen. It is accessed using the `px_order` variable in the configuration structure, or by calling the `scr_px_order_set()` function. Basically the `px_order` array must have one index for each pixel on the screen. These indexes are in byte position units (a byte position is computed as `3 * pixel_position`). During the drawing process, if set, these indexes are accessed to alter where each pixel is drawn. For example if the `px_order` array points to the following contents:

```C
static const uint32_t px_order[] = { 3 * 10, 3 * 20, 3 * 30, 3 * 40, ... };
```

Then during drawing the first pixel in the map window covered by the scroll values will be drawn to screen pixel number 10, the second pixel will be drawn to screen pixel 20, and so on. This can be used to achieve many types of screen distortion effects, like rotation, scanline-like effects, or for adapting to screens with non-linear framebuffers (like e.g. in the typical led-strip matrices in which each line has reverse order).

For example, if you have a 5x5 screen and you want to achieve a 90 degree rotation, set `px_order` as follows:

```C
static const uint32_t px_order[] = {
	 3 * 4,  3 * 9, 3 * 14, 3 * 19, 3 * 24,
	 3 * 3,  3 * 8, 3 * 13, 3 * 18, 3 * 23,
	 3 * 2,  3 * 7, 3 * 12, 3 * 17, 3 * 22,
	 3 * 1,  3 * 6, 3 * 11, 3 * 16, 3 * 21,
	 3 * 0,  3 * 5, 3 * 10, 3 * 15, 3 * 20
};
```

This will cause the first row of pixels to be drawn to the rightmost (fifth) column, the second row to be drawn on the fourth column and so on.

If the buffer is non linear and each line has reverse order (typical on ws2812b LED matrix displays), you can get the buffer to render correctly using the following `px_order`:

```C
static const uint32_t px_order[] = {
	 3 * 0,  3 * 1,  3 * 2,  3 * 3,  3 * 4,
	 3 * 9,  3 * 8,  3 * 7,  3 * 6,  3 * 5,
	 3 * 10, 3 * 11, 3 * 12, 3 * 13, 3 * 14,
	 3 * 19, 3 * 18, 3 * 17, 3 * 16, 3 * 15,
	 3 * 20, 3 * 21, 3 * 22, 3 * 23, 3 * 24
};
```
It's up to your imagination to find more creative ways to exploit this feature!

# License

This module is licensed with NO WARRANTY under the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/).

