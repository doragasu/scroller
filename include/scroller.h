/************************************************************************//**
 * \brief Scroller implementation. Allows scrolling a 2D map in a 2D window.
 *
 * Both 2D map and window must be 24-bit per pixel.
 ****************************************************************************/

#ifndef _SCROLLER_H_
#define _SCROLLER_H_

#include <stdint.h>

/// Defines the dimensions of a 2D plane
struct scr_dim_2d {
	uint32_t width;
	uint32_t height;
};

/// Scroller module configuration
struct scr_cfg {
	struct scr_dim_2d map;    ///< Background map dimensions
	struct scr_dim_2d screen; ///< Screen dimensions
	const uint8_t *map_buf;   ///< Map buffer, 24-bits per pixel
	uint8_t *screen_buf;      ///< Screen buffer, 24-bits per pixel
	/// Optional: for custom pixel ordering, read README.md for details
	const uint32_t *px_order;
	/// Optional: if set, wrap at specified column instead of map end
	uint32_t wrap_width;
};

#ifdef __cplusplus
extern "C" {
#endif

/// Scroller module handler
typedef struct scr* scr_handle;

/************************************************************************//**
 * \brief Initialize a scroller.
 *
 * \param[in] cfg Scroll module configuration.
 *
 * \return The scroller handler, or NULL if not enough memory is available.
 ****************************************************************************/
scr_handle scr_init(const struct scr_cfg *cfg);

/************************************************************************//**
 * \brief De-initialize a scroller, freeing its resources.
 *
 * \param[in] scr Scroller handler to deinitialize.
 ****************************************************************************/
void scr_deinit(scr_handle scr);

/************************************************************************//**
 * \brief Draw the window on the screen buffer, using the previously set
 * brightness and coordinates.
 *
 * \param[in] scr Scroller used to draw the window.
 * \param[in] x   Horizontal coordinate, grows from left to right.
 * \param[in] y   Vertical coordinate, grows from top to bottom.
 ****************************************************************************/
void scr_draw(scr_handle scr);

/************************************************************************//**
 * \brief Get the current configuration for specified scroller.
 *
 * \param[in] scr Scroller to obtain config from.
 *
 * \return The specified scroller configuration.
 ****************************************************************************/
struct scr_cfg *scr_cfg_get(scr_handle scr);

/************************************************************************//**
 * \brief Set the configuration for the specified scroller.
 *
 * Useful if you want to change configuration parameters after initialization.
 *
 * \param[in] scr Scroller to configure.
 * \param[in] cfg New configuration to set.
 ****************************************************************************/
void scr_config_set(scr_handle scr, const struct scr_cfg *cfg);

/************************************************************************//**
 * \brief Sets the plane scroll coordinates.
 *
 * \param[in] scr Scroller handle to set scroll values.
 * \param[in] x   Horizontal coordinate, grows from left to right.
 * \param[in] y   Vertical coordinate, grows from top to bottom.
 ****************************************************************************/
void scr_scroll_set(scr_handle scr, uint32_t x, uint32_t y);

/************************************************************************//**
 * \brief Gets the current scroll coordinates.
 *
 * \param[in]  scr Scroller handle get coordinates from.
 * \param[out] x   Horizontal coordinate, grows from left to right.
 * \param[out] y   Vertical coordinate, grows from top to bottom.
 ****************************************************************************/
void scr_scroll_get(scr_handle scr, uint32_t *x, uint32_t *y);

/************************************************************************//**
 * \brief Set the pixel ordering configuration for specified scroller.
 *
 * \param[in] scr      Scroller to configure.
 * \param[in] px_order Pixel ordering (see README.md for details).
 ****************************************************************************/
void scr_px_order_set(scr_handle scr, const uint32_t *px_order);

/************************************************************************//**
 * \brief Get configured brightness level.
 *
 * \param[in] scr Scroller to query for brightness level.
 *
 * \return Brightness level ranging from 0 (lowest) to 255 (highest)
 ****************************************************************************/
uint_fast8_t scr_brightness_get(scr_handle scr);

/************************************************************************//**
 * \brief Configure brightness level.
 *
 * \param[in] scr   Scroller to configure.
 * \param[in] level Brightness level to set, from 0 (lowest) to 255 (highest).
 ****************************************************************************/
void scr_brighthess_set(scr_handle scr, uint_fast8_t level);

#ifdef __cplusplus
}
#endif

#endif /*_SCROLLER_H_*/

